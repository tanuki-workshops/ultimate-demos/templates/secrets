# Secret detection

- https://docs.gitlab.com/ee/user/application_security/secret_detection/
- https://about.gitlab.com/direction/secure/static-analysis/secret-detection/

## Historic scan

To configure Secret Detection to use non-default behavior, you override the `secret_detection` job definition and add variables inside it.

```yaml
secret_detection:
  tags: [ saas-linux-large-amd64 ]
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "true"
```
